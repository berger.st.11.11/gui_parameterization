import os, sys

from tkinter import *
import tkinter
import customtkinter

from matplotlib.backends.backend_tkagg import (FigureCanvasTkAgg, NavigationToolbar2Tk)
import numpy as np  

import matplotlib.animation as animation
from matplotlib.widgets import Slider, Button
from matplotlib.backend_bases import key_press_handler
from matplotlib.figure import Figure
import matplotlib as mpl
from matplotlib import pyplot as plt
import matplotlib.patches as patches
from scipy.interpolate import *
import numpy as np
import torch

from export_non_stationary_PINN_result import export_non_stationary_PINN_result

import pickle
import dill

# Set paths to known folder structure for different PINNs and lib files.
file_path = os.path.dirname(__file__)
# Absolute path to lib folder
parent_dir = '/'.join([file_path,'..'])
sys.path.append(parent_dir)



## Global Variables ##
global current_cycle, segment_counter


### SPLINE EDITOR ###
def update(val):
    global yvals
    global xvals
    global spline
    # update curve

    l.set_ydata(yvals)
    l.set_xdata(xvals)
    spline =interp1d(xvals, yvals, kind="linear",bounds_error=False)
    m.set_ydata(spline(X))
    # redraw canvas while idle
    fig_spline_editor.canvas.draw_idle()

def reset(event):
    global yvals
    global spline
    #reset the values
    xvals = x
    yvals = func(x)

    spline =  interp1d(x, yvals, kind='linear',bounds_error=False)
    l.set_ydata(yvals)
    m.set_ydata(spline(X))
    # redraw canvas while idle
    fig_spline_editor.canvas.draw_idle()

def button_press_callback(event):
    'whenever a mouse button is pressed'
    global pind
    if event.inaxes is None:
        return
    if event.button != 1:
        return
    pind = get_ind_under_point(event) 

def button_release_callback(event):
    'whenever a mouse button is released'
    global pind
    if event.button != 1:
        return
    pind = None
    canvas_spline_editor.draw()

def get_ind_under_point(event):
    'et the index of the vertex under point if within epsilon tolerance'

    # display coords
    #print('display x is: {0}; display y is: {1}'.format(event.x,event.y))
    t = ax_spline_editor.transData.inverted()
    tinv = ax_spline_editor.transData 
    xy = t.transform([event.x,event.y])
    #print('data x is: {0}; data y is: {1}'.format(xy[0],xy[1]))
    xr = np.reshape(x,(np.shape(x)[0],1))
    yr = np.reshape(yvals,(np.shape(yvals)[0],1))
    xy_vals = np.append(xr,yr,1)
    xyt = tinv.transform(xy_vals)
    xt, yt = xyt[:, 0], xyt[:, 1]
    d = np.hypot(xt - event.x, yt - event.y)
    indseq, = np.nonzero(d == d.min())
    ind = indseq[0]

    #print(d[ind])
    if d[ind] >= epsilon:
        ind = None
    
    #print(ind)
    return ind

def motion_notify_callback(event):
    'on mouse movement'
    global yvals
    if pind is None:
        return
    if event.inaxes is None:
        return
    if event.button != 1:
        return
    
    #update yvals
    # print('motion x: {0}; y: {1}'.format(event.xdata,event.ydata))
    yvals[pind] = event.ydata 
    # xvals[pind] = event.xdata

    sliders[pind].set_val(yvals[pind])
    # update curve via sliders and draw
    fig_spline_editor.canvas.draw_idle()
    canvas_spline_editor.draw()


### Generation of Cycle ###

def add():
    global segment_counter, current_cycle
       
    duration = float(cycle_generator_settings_time_input.get()) if float(cycle_generator_settings_time_input.get()) <= 7.5 else 7.5
    inflow_speed = 4.5*cycle_generator_settings_slider.get()
    temperatures =  yvals.tolist()
   
    # add a dictionary entry to the current cycle
    current_cycle[f"segment_{segment_counter}"] = {"duration": duration, "inflow_speed": inflow_speed, "temperatures": temperatures}
    segment_counter += 1
    
    update_drive_cycle_display()
    
   
   
    
def refresh():
    global segment_counter, current_cycle
    global canvas_drive_cycle_display, ax_drive_cycle_display, ax_drive_cycle_display_velocity
    global temperature_values, velocity_values, duration_values

    temperature_values, velocity_values, duration_values = [],[],[]

    current_cycle = {}
    segment_counter = 0
    
    ax_drive_cycle_display.clear()
    ax_drive_cycle_display_velocity.clear()
    canvas_drive_cycle_display.draw()
        

def update_drive_cycle_display():
    global canvas_drive_cycle_display, ax_drive_cycle_display, ax_drive_cycle_display_velocity, drive_cycle_display
    global temperature_values, velocity_values, duration_values

    delta_t = 2.5

    temperature_values, velocity_values, duration_values = [],[],[]
    
    for i in range(segment_counter):
        duration_values.append(current_cycle[f"segment_{i}"]["duration"])
        velocity_values.append(current_cycle[f"segment_{i}"]["inflow_speed"])
        temperature_values.append(current_cycle[f"segment_{i}"]["temperatures"])
    
    # Generate pairs of (time, temperature)
    temperature_pairs = []
    for i in range(segment_counter):
        if i == 0:
            t_start_segment = 0
        else: 
            t_start_segment = sum([duration_values[j] for j in range(i)])
 
  
        for j,v in enumerate(temperature_values[i]):
        
            if j*delta_t > duration_values[i]:
                if duration_values[i] % delta_t != 0:
                    temperature_pairs.append((t_start_segment+duration_values[i],temperature_values[i][j-1]))
                break
            else:
                temperature_pairs.append((t_start_segment+j*delta_t,v))
    
    ax_drive_cycle_display.plot(*zip(*temperature_pairs), c="r", marker="o", markersize=10)
    ax_drive_cycle_display.set_xlabel("Time [s]")
    ax_drive_cycle_display.set_ylabel("Temperature [K]", color="red")
    ax_drive_cycle_display.tick_params(axis='y', labelcolor="red")
    ax_drive_cycle_display.set_ylim([245,355])
    
    
    # Add second axis for the velocity
    # Generate speed data for velocity plot
    for i in range(segment_counter):
        if i == 0:
            t_start_segment = 0
        else: 
            t_start_segment = sum([duration_values[j] for j in range(i)])
        
        t_end_segment = sum([duration_values[j] for j in range(i+1)])
        
        t = [t_start_segment, t_end_segment]
        v = [velocity_values[i], velocity_values[i]]

        ax_drive_cycle_display_velocity.plot(t,v, color="blue", linewidth=5)
        
        # add boxes for the segments
        
        color = "grey" if i%2 == 0 else "white"
        patch = patches.Rectangle((t_start_segment,0), width=duration_values[i], height=4.5, color=color ,alpha =0.2)
        ax_drive_cycle_display_velocity.add_patch(patch)
    
    
    
    ax_drive_cycle_display_velocity.set_ylabel("U_max [m/s]", color="blue")
    ax_drive_cycle_display_velocity.tick_params(axis='y', labelcolor="blue")
    ax_drive_cycle_display_velocity.invert_yaxis() # muss 2 mal hier sein
    ax_drive_cycle_display_velocity.invert_yaxis()

    drive_cycle_display.tight_layout()
    canvas_drive_cycle_display.draw()

def get_fitting_flow_pinn(u_max:float):
    global flowFields

    u_max = u_max/4.5

    if u_max < 0.3:
        return flowFields["Re10"]
    elif u_max < 0.6:
        return flowFields["Re100"]
    else:
        return flowFields["Re300"]

def get_fitting_heat_pinn(u_max:float):
    global pinns
    
    u_max = u_max/4.5

    if u_max < 0.3:
        return pinns["Re10"]
    elif u_max < 0.6:
        return pinns["Re100"]
    else:
        return pinns["Re300"]

def call_export_function(i):
    global current_cycle, segment_counter
    global temperature_values, velocity_values, duration_values
    global pinns, flowFields

    # Add extra case for the starting process
    if i == 0:
        export_file_counter = 0
        parameters = temperature_values[i]
        parameters.append(temperature_values[i][-1])
        t_end = duration_values[i]
        flow_pinn = get_fitting_flow_pinn(velocity_values[i])
        heat_pinn = get_fitting_heat_pinn(velocity_values[i])

        plotting_settings= {"config_path": "/".join([file_path, "config.yml"]), 
                            "mesh_file_plot_path": "/".join([file_path, "Meshes/cylinder_gereon.msh"]),
                            "parameters": parameters,
                            "export_path_vtu": "/".join([file_path, "Output"]),
                            "t_start": 0,
                            "t_end": t_end,
                            "t_steps": t_end*20,
                            "flow_pinn": flow_pinn,
                            "starting_counter": export_file_counter}

        export_file_counter += t_end*10

        export_non_stationary_PINN_result(heat_pinn, plotting_settings, "drive_cycle_output")

    else:
        export_file_counter = sum([duration_values[j] for j in range(i)])*10
        parameters = [temperature_values[i-1][-1]]
        parameters.append(temperature_values[i][:4])
        t_end = duration_values[i] + 2.5
        flow_pinn = get_fitting_flow_pinn(velocity_values[i])
        heat_pinn = get_fitting_heat_pinn(velocity_values[i])

        plotting_settings= {"config_path": "/".join([file_path, "config.yml"]), 
                            "mesh_file_plot_path": "/".join([file_path, "Meshes/cylinder_gereon.msh"]),
                            "parameters": parameters,
                            "export_path_vtu": "/".join([file_path, "Output"]),
                            "t_start": 2.5,
                            "t_end": t_end,
                            "t_steps": (t_end-2.5)*20,
                            "flow_pinn": flow_pinn,
                            "starting_counter": export_file_counter}

        export_file_counter += t_end*10

        export_non_stationary_PINN_result(heat_pinn, plotting_settings, "drive_cycle_output")

def generate():
    global current_cycle, segment_counter
    global temperature_values, velocity_values, duration_values
    global pinns, flowFields

    for i in range(segment_counter):
        print(f"Generating files for segment {i+1}")
        call_export_function(i)
        
    print("Generated all files for the drive cycle!")

def init_pinns():
    global pinns, flowFields

    pinn_folder = "/".join([file_path, "PINNs"])
    flowFields_folder = "/".join([file_path, "FlowFields"])

    pinns, flowFields = {}, {}
    
    pinns["Re10"] = dill.load(open("/".join([pinn_folder, "heatPINN_Re10"]), "rb"))
    pinns["Re100"] = dill.load(open("/".join([pinn_folder, "heatPINN_Re100"]), "rb"))
    pinns["Re300"] = dill.load(open("/".join([pinn_folder, "heatPINN_Re300"]), "rb"))

    flowFields["Re10"]  = pickle.load(open('/'.join([flowFields_folder, "10.p"]), "rb"))
    flowFields["Re100"] = pickle.load(open('/'.join([flowFields_folder, "100.p"]), "rb"))
    flowFields["Re300"] = pickle.load(open('/'.join([flowFields_folder, "300.p"]), "rb"))

### Starting with layout ###

customtkinter.set_appearance_mode("dark")  # Modes: system (default), light, dark
customtkinter.set_default_color_theme("blue")  # Themes: blue (default), dark-blue, green

root = customtkinter.CTk()
root.title("GUI for Parameterization of PINNs")
root.geometry("1400x1100")

# Define Layout
cycle_generator_frame = customtkinter.CTkFrame(root, width=1400, height=400)
cycle_generator_frame.pack(anchor=N, padx=10, pady=10)
cycle_generator_frame.pack_propagate(0)

cycle_generator_frame.grid_rowconfigure(4, weight=1)
cycle_generator_title = customtkinter.CTkLabel(cycle_generator_frame, text="Cycle Generator", font=customtkinter.CTkFont("bold", 20))
cycle_generator_title.pack(anchor=NW)


cycle_generator_settings = customtkinter.CTkFrame(cycle_generator_frame, width=1300, height=350)
cycle_generator_settings.pack(anchor=N, padx=10, pady=10)
cycle_generator_settings.pack_propagate(0)

current_cycle_display_frame = customtkinter.CTkFrame(root, width=1400, height=600)
current_cycle_display_frame.pack(anchor=N, padx=10, pady=10)
current_cycle_display_frame.pack_propagate(0)

current_cycle_display_label = customtkinter.CTkLabel(current_cycle_display_frame, text="Current Cycle", font=customtkinter.CTkFont("bold", 20))
current_cycle_display_label.pack(anchor=NW, padx=10, pady=10)


#### Settings for Cycle Generator
# Time settings
cycle_generator_settings_time_frame = customtkinter.CTkFrame(cycle_generator_settings, width=200, height=350)
cycle_generator_settings_time_frame.pack(anchor=W, padx=10, pady=10, side=LEFT)

cycle_generator_settings_time_label = customtkinter.CTkLabel(cycle_generator_settings_time_frame, text="Duration", font=customtkinter.CTkFont("bold", 20))
cycle_generator_settings_time_label.pack(anchor=N, padx=10, pady=10)

cycle_generator_settings_time_input = customtkinter.CTkEntry(cycle_generator_settings_time_frame, width=100, height=50)
cycle_generator_settings_time_input.pack(anchor=N, padx=10, pady=10)

# Speed settings
cycle_generator_settings_slider_frame = customtkinter.CTkFrame(cycle_generator_settings, width=200, height=350)
cycle_generator_settings_slider_frame.pack(anchor=W, padx=10, pady=10, side=LEFT)

cycle_generator_settings_slider_label = customtkinter.CTkLabel(cycle_generator_settings_slider_frame, text="InflowSpeed", font=customtkinter.CTkFont("bold", 20))
cycle_generator_settings_slider_label.pack(anchor=N, padx=10, pady=10)

cycle_generator_settings_slider = customtkinter.CTkSlider(cycle_generator_settings_slider_frame, from_=0, to=1, orientation="vertical")
cycle_generator_settings_slider.pack(anchor=N, padx=10, pady=10)

# Temperature settings
cycle_generator_settings_temperature_frame = customtkinter.CTkFrame(cycle_generator_settings, width=800, height=350)
cycle_generator_settings_temperature_frame.pack(anchor=W, padx=10, pady=10, side=LEFT)	
cycle_generator_settings_temperature_frame.pack_propagate(0)

cycle_generator_settings_temperature_label = customtkinter.CTkLabel(cycle_generator_settings_temperature_frame, text="CylinderTemperature", font=customtkinter.CTkFont("bold", 20))
cycle_generator_settings_temperature_label.pack(anchor=N, padx=10, pady=10)

cycle_generator_settings_temperature_spline = customtkinter.CTkFrame(cycle_generator_settings_temperature_frame, width=800, height=350)
cycle_generator_settings_temperature_spline.pack(anchor=W, padx=10, pady=10, side=LEFT)	
cycle_generator_settings_temperature_spline.pack_propagate(0)

# set up a plot
px = 1/plt.rcParams['figure.dpi']  # pixel in inches
fig_spline_editor = Figure(figsize=(1000*px, 280*px), dpi=100)
ax_spline_editor = fig_spline_editor.add_subplot()

canvas_spline_editor = FigureCanvasTkAgg(fig_spline_editor, master=cycle_generator_settings_temperature_spline)  # A tk.DrawingArea.

fig_spline_editor.canvas.mpl_connect('button_press_event', button_press_callback)
fig_spline_editor.canvas.mpl_connect('button_release_event', button_release_callback)
fig_spline_editor.canvas.mpl_connect('motion_notify_event', motion_notify_callback)

canvas_spline_editor.get_tk_widget().pack(side=tkinter.TOP, fill=tkinter.BOTH, expand=True)

T_START = 300
T_MIN   = 250
T_MAX   = 350

#get a list of points to fit a spline to as well
N = 4
xmin = 0 
xmax = 7.5
x = np.linspace(xmin,xmax,N)

#spline fit
xvals = x
yvals = T_START * np.ones(x.shape)
spline = interp1d(xvals, yvals, kind="linear", bounds_error=False)

pind = None #active point
epsilon = 5 #max pixel distance

X = np.arange(0,xmax+1,0.01)
l, = ax_spline_editor.plot (xvals,yvals,color='r',linestyle='none',marker='o',markersize=8, zorder=10)
m, = ax_spline_editor.plot (X, spline(X), 'r-')

ax_spline_editor.set_yscale('linear')
ax_spline_editor.set_xlim(-0.1, xmax+0.1)
ax_spline_editor.set_ylim(T_MIN-1,T_MAX+1)
ax_spline_editor.set_xlabel('Time [s]')
ax_spline_editor.set_ylabel('Temperature [K]')
ax_spline_editor.grid(True)
ax_spline_editor.yaxis.grid(True,which='minor',linestyle='--')

sliders = []
for i in np.arange(N):
    axamp = plt.axes([0.84, 0.8-(i*0.05), 0.12, 0.02])
    # Slider
    s = Slider(axamp, 'p{0}'.format(i), 0, 10, valinit=yvals[i])
    sliders.append(s)

    
for i in np.arange(N):
    #samp.on_changed(update_slider)
    sliders[i].on_changed(update)

axres = plt.axes([0.84, 0.8-((N)*0.05), 0.12, 0.02])
bres = Button(axres, 'Reset')
bres.on_clicked(reset)

# Controls
cycle_generator_settings_controls_frame = customtkinter.CTkFrame(cycle_generator_settings, width=200, height=350)
cycle_generator_settings_controls_frame.pack(anchor=W, padx=10, pady=80, side=LEFT)
cycle_generator_settings_controls_frame.pack_propagate(0)

cycle_generator_settings_controls_label = customtkinter.CTkLabel(cycle_generator_settings_controls_frame, text="Controls", font=customtkinter.CTkFont("bold", 20))
cycle_generator_settings_controls_label.pack(anchor=NW, padx=10, pady=10)

reset_button = customtkinter.CTkButton(cycle_generator_settings_controls_frame, command=refresh, text="Refresh" ).pack(anchor=NW, pady=10, padx=10)
add_button = customtkinter.CTkButton(cycle_generator_settings_controls_frame, command=add, text="Add" ).pack(anchor=NW,pady=10, padx=10)
generate_button = customtkinter.CTkButton(cycle_generator_settings_controls_frame, command=generate, text="Generate" ).pack(anchor=NW,pady=10, padx=10)


#### Settings for Cycle Display ####
current_cycle_display_plot = customtkinter.CTkFrame(current_cycle_display_frame, width=1400, height=500)
current_cycle_display_plot.pack(anchor=NW, padx=10, pady=10)
current_cycle_display_plot.pack_propagate(0)

# set up a plot
drive_cycle_display = Figure(figsize=(1400*px, 500*px), dpi=100)
ax_drive_cycle_display = drive_cycle_display.add_subplot()
ax_drive_cycle_display_velocity = ax_drive_cycle_display.twinx()

canvas_drive_cycle_display = FigureCanvasTkAgg(drive_cycle_display, master=current_cycle_display_plot)
canvas_drive_cycle_display.get_tk_widget().pack(side=tkinter.TOP, fill=tkinter.BOTH, expand=True)


if __name__ == "__main__":
    current_cycle = {}
    segment_counter = 0

    init_pinns()

    # run the gui
    root.mainloop()
