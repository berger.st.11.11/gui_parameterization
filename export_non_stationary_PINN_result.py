"""
This function creates an vtk output of the given PINN and parameter
"""
import torch
import meshio
import numpy as np
import os

from alive_progress import alive_bar
import time

class FlowField:
    '''
    The FlowField class saves the data about the flow field, that is used in the conv.-diff. equation
    '''

    def __init__(self, itp, x_min=None, x_max=None, y_min=None, y_max=None, z_min=None, z_max=None, u_x_min=None, u_x_max=None, u_y_min=None, u_y_max=None, u_z_min=None, u_z_max=None):
        '''
        Constructor of the FlowField class. Any interpolator with a `__call__(self, t, x, y, z)` method can be used. However it should use the pytorch tensor datatype.
        '''
        self.itp = itp
        if x_min is not None:
            self.x_min = x_min
            self.y_min = y_min
            self.z_min = z_min
            self.x_max = x_max
            self.y_max = y_max
            self.z_max = z_max

            self.u_x_min = u_x_min
            self.u_x_max = u_x_max 
            self.u_y_min = u_y_min
            self.u_y_max = u_y_max
            self.u_z_min = u_z_min
            self.u_z_max = u_z_max

    def eval(self, t, x, y, z):
        '''
        This method is used to evaluate the flow field at the `t, x, y, z` point
        '''
        return self.itp(t, x, y, z)

    def __call__(self, t, x, y, z):
        return self.itp(t, x, y, z)

def get_parameterization_dim(parameterization_config):
    # Get number of boundary value parameter that get parameterized
    n_parameterization_dim = 0
    for parameterization_boundary in parameterization_config['Parameterization'].keys():
        for parameter_to_vary in parameterization_config['Parameterization'][parameterization_boundary]['Parameters_to_vary'].keys():
            n_parameterization_dim += 1 

    return n_parameterization_dim


def export_non_stationary_PINN_result(pinn, plotting_settings:dict, export_name:str):
    
    #########################
    # Read parameters from the various configs:
    #########################
    config_path         = plotting_settings["config_path"]
    mesh_file_plot_path = plotting_settings["mesh_file_plot_path"]
    parameters          = plotting_settings["parameters"]
    export_path_vtu     = plotting_settings["export_path_vtu"]
    t_start             = plotting_settings["t_start"]
    t_end               = plotting_settings["t_end"]
    t_steps             = plotting_settings["t_steps"]
    flow_pinn           = plotting_settings["flow_pinn"]
    starting_counter    = plotting_settings["starting_counter"]


    #########################
    # Export Solution to vtk
    #########################

    # Check if export dir exists, else create
    if not os.path.exists(export_path_vtu):
        os.makedirs(export_path_vtu)
    
    
    # Delete previous data if starting_counter is 0
    if starting_counter == 0:
        filelist = [f for f in os.listdir(export_path_vtu) if f.endswith(".vtu")]
        for f in filelist:
            os.remove("/".join([export_path_vtu,f]))

    mesh = meshio.read(mesh_file_plot_path)
    time_points_export = np.linspace(t_start, t_end, int(t_steps))

    with alive_bar(len(time_points_export)) as bar:
        for i, t_cur in enumerate(time_points_export):
            # print("current time: \t" + str(t))

            # Compute predictions for t
            x = mesh.points[:,0]
            y = mesh.points[:,1]
            t = t_cur * np.ones((mesh.points.shape[0],1))

            x   = x.reshape([len(x),1])
            y   = y.reshape([len(y),1])
            t   = t.reshape([len(t),1])

            input_data = np.concatenate([x , y, t],axis=1)

            parameters = parameters[:5]
            if len(parameters) != 0:
                for parameter in parameters:
                    input_data = np.concatenate((input_data, np.array(parameter * np.ones((input_data[:, 0].shape[0], 1)))), axis=1)
            
            
            tmp_idx = input_data.shape[1]
            # TODO add two dim for u and v
            input_data = np.concatenate((input_data, np.array(np.zeros((input_data[:, 0].shape[0], 2)))), axis=1)
            
            data = np.concatenate([input_data[:, :2], input_data[:,4:5]], axis=1)
            data = torch.from_numpy(data).float()
            # velocity_from_pinn  = flow_pinn.forward_for_export(data.to("cuda:0"))
            # input_data[:,5:] = velocity_from_pinn.cpu().detach().numpy()[:,0:2]
            t_data = input_data[:, 2]
            x_y_data = input_data[:,:2]
            z_data = np.zeros(input_data.shape[0])
            data = np.concatenate([t_data.reshape(-1,1), x_y_data, z_data.reshape(-1,1)], axis=1)
            velocity = flow_pinn.itp(data)
            input_data[:,tmp_idx:] = velocity[:,0:2]
            
            try:
                results = pinn.forward_for_export(torch.from_numpy(input_data).float().to(pinn.device))
            except:
                results = pinn.forward(torch.from_numpy(input_data).float().to(pinn.device))
            
            point_data = {}
            for j,column in enumerate(results.T):
                point_data["point_data_"+str(j)] = results[:,j].detach().cpu().numpy()


            # Write to file
            meshio.write_points_cells( export_path_vtu +"/" + export_name + "_{}.vtu".format(int(i+starting_counter)), mesh.points, mesh.cells, point_data)
            bar()
